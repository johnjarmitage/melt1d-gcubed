% 1-D melting model 
% John Armitage (IPGP)
% Kenni Petersen (Aarhus)

% time,glacier,mprd,merupt,incCMelt,topCMelt,carbonerupt,phi,T,coord] = melt1d_TVD_ice(10e3,10^20.5,200e3,1450,30e-3);

function [time,mprd,incCMelt,T,coord] = melt1d_gcubed(Tp)
global zMin zMax NN params;
close all force

plotend   = 'on';
REE_comp  = 'on';

tstep = 1e3;

setupProblem();
setupParams(tstep,Tp);

% allocate arrays
T          = zeros(NN,1); % temperature
T_old      = zeros(NN,1); % temperature
dFdP       = zeros(NN,1); % melt production
Dep        = zeros(NN,1); % depletion
Xwater     = params.water*ones(NN,1); % depletion
phi        = zeros(NN,1); % porosity
velphi     = zeros(NN,1); % porosity velocity
velphiC    = zeros(NN,1); % melt velocity
velS       = zeros(NN,1); % solid velocity

latentheat = zeros(NN,1);


% Calculate coordinates
range = zMax - zMin;
dz     = range / (NN-1);
for i = 1:NN
    coord(i) = zMin + (i-1)*dz;
end
Pres   = params.rhom*params.g*params.lz*coord';

% initial composition
if (strcmp(REE_comp,'on') == 1)
    [CMantle,CMelt,nREE] = setupComp();
else
    Cmelt  = 0;
end

% initial condition
T         = ones(NN,1);
Ts        = get_Ts(Pres,Dep);
Twet      = (Ts + params.aX.*(0.01.*Xwater).^params.bX)./params.T0;
T(T>Twet) = Twet(T>Twet);
T(T>1)    = 1;

% upwards mean velocity
year = 365*24*60*60;
Vel = -5e-3*params.lz./params.kappa/year*ones(1,NN);  % uniform upwards velocity
%Vel  = -2.582e-3*exp(-0.0258e-3*coord*params.lz)*params.lz./params.kappa/year;

% Perform itterations
params.iterate  = 1; % global

t        = 0;
w_mid    = 0;
tmax     = params.max_time;
tmaxt    = round(tmax) + 1;
time     = zeros(1,tmaxt);
mprd     = zeros(1,tmaxt);
if (strcmp(REE_comp,'on') == 1)
    incCMelt = zeros(nREE,tmaxt);
end

while params.iterate > 0
    
    t = t+1;

    dt = params.dt;
    if t == 1
        time(t) = dt;
    else
        time(t) = time(t-1) + dt;
    end
                 
    % First advect with mean velocity
    T      = advection_1d(coord,T,Vel,dz,dt,t,'upwind');
        
    % First advect solids
    Dep    = advection_1d(coord,Dep,Vel,dz,dt,t,'TVD');
    Xwater = advection_1d(coord,Xwater,Vel,dz,dt,t,'TVD');
    
    if (strcmp(REE_comp,'on') == 1)
        for i = 1:nREE
            CMantle(i,:) = advection_1d(coord,CMantle(i,:)',Vel,dz,dt,t,'TVD');
            CMelt(i,:) = advection_1d(coord,CMelt(i,:)',Vel,dz,dt,t,'TVD');
        end
    end
               
    % Second diffusion         
    T = diffusion_1d(T,dz,dt,t);
                
    % Calculate melt
    [T,dFdP,Dep,Xwater] = melting(T,Pres,Dep,Xwater);
               
    % Calculate melt production/eruption rate
    mprd(t) = abs(mean(Vel))*trapz(dFdP)*dz;
    
    % Update composition
    if (strcmp(REE_comp,'on') == 1)
        [CMantle,CMelt] = REEcomp(T,Pres,coord,dFdP,CMantle,CMelt);
        for i = 1:nREE
            incCMelt(i,t) = trapz(CMelt(i,:).*dFdP')/trapz(dFdP);
        end
    end
       
    % done now?
    
    % I guess so, so lets update time in case dt changed
    if t == 1
        time(t) = dt;
    else
        time(t) = time(t-1) + dt;
    end
    if time(t) > tmax
        params.iterate = 0;
    end
%     if t > 2
%         params.iterate = 0;
%     end
    
end

if (strcmp(plotend,'on') == 1)
    fig2 = figure(102);
    set(fig2,'position',[120 120 600 600]);
    subplot(1,2,1)
    hold on, plot(params.T0*T,1e-3*params.lz*coord,'r');
    plot(get_Ts(Pres,Dep),1e-3*params.lz*coord,'k--');
    plot(get_Ts(Pres,Dep) + params.aX.*(0.01.*Xwater).^params.bX,1e-3*params.lz*coord,'k');
    set(gca,'xlim',[800 1600]);
    set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
    xlabel('Temperature (^{\circ}C)');
    ylabel('Depth (km)');
    axis ij
    
    subplot(1,2,2)
    hold on, plot(1e2*Dep,1e-3*params.lz*coord,'o-k');
    xlabel('Depletion (%)');
    ylabel('Depth (km)');
    set(gca,'ylim',[1e-3*params.lz*min(coord) 150]);
    axis ij
  
    if (strcmp(REE_comp,'on') == 1)
        fig4 = figure(104);
        set(fig4,'position',[160 160 600 600],'PaperPositionMode','auto');
        subplot(2,1,1)
        year   = 365*24*60*60;
        perMyr = 1e3*year*params.kappa/(params.lz);
        hold on, plot(params.myr*time,perMyr*mprd,'k')
        xlabel('Time (Myr)');
        ylabel('Mprd (km/Myr^{-1})');
        
        subplot(2,1,2)
        plot(params.myr*time,incCMelt(1,:)./incCMelt(13,:),'k')
        xlabel('Time (Myr)');
        ylabel('La/Yb');
%         hold on;
%         [AX,H1,H2] = plotyy(params.myr*time,incCMelt(1,:),...
%             params.myr*time,incCMelt(13,:));
%         xlabel('Time (Myr)');
%         set(get(AX(1),'Ylabel'),'String','La')
%         set(get(AX(2),'Ylabel'),'String','Sm')
%         set(H1,'LineStyle','--')
%         set(H2,'LineStyle','-')
        
        print(fig4,'-dpng','plots/simple_mod1300')
    end
end

end





%========================================================================
% MELTING
%========================================================================
function [] = setupProblem
%========================================================================
global zMin zMax NN
zMin = 5/30;
zMax = 1;
NN   = 1001;

end % End of function setupProblem()

%========================================================================
function [] = setupParams(tstep,Tp)
%========================================================================
global zMin zMax NN params;

year   = 365*24*60*60;
tstep  = tstep*year;
tmax   = 10e6*year;

params.lz     = 300e3;   % system depth metres
params.T0     = Tp;
params.g      = 9.8;
params.kappa  = 1e-6;  % thermal diffusion coefficient
params.Cp     = 1.2e3; % heat capacity
params.deltaS = 400;   % entropy change upon1 melting
params.alpha  = 3e-5;  % thermal activation energy
params.rhom   = 3100;  % mantle density
params.rhoi   = 920;   % ice density
params.rhol   = 2800;  % melt density

params.Tsub   = 200;
params.L      = 550;
params.Fdry   = 0.02;
params.dF     = 1e-6;

% Coefficients for parameterization of wet melting (Katz et al. 2003)
params.Ts0     = 1081;
params.dTdP_F  = 132*1e-9;  % solidus pressure gradient 
params.dTdF_Ps = 300;       % solidus depletion gradient spinel lherzolite
params.dTdF_Ph = 300;       % solidus depletion gradient harzburgite
params.water   = 200;
params.Dbulk   = 0.007;     % partition coefficient for water
params.aX      = -43;
params.bX      = 0.75;

params.dt       = params.kappa*tstep/(params.lz*params.lz);
params.max_time = params.kappa*tmax/(params.lz*params.lz);

params.myr            = 1e-6*params.lz*params.lz/(params.kappa*year);

end

%========================================================================
function vari = advection_1d(x,var,U,dz,dt,t,method)
%========================================================================
global NN params
if strcmp(method,'upwind') == 1 
    vel = U';
    if (t == 1)
        params.advA   = sparse(diag((1+dt/dz*vel(1:NN))) +...
            diag(-dt/dz*vel(1:NN-1),1));
        A = params.advA;
    elseif (dt ~= params.dt)
        A   = sparse(diag((1+dt/dz*vel(1:NN))) +...
            diag(-dt/dz*vel(1:NN-1),1));
    else
        A = params.advA;
    end
    % boundary conditions
    % top zero gradient
    
    % top fixed value
    A(1,1)   = 1;
    A(1,2)   = 0;
    % bottom fixed value
    A(NN,NN) = 1;
    vari     = A*var;
elseif strcmp(method,'TVD') == 1
    % lets be general and assume we don't know if vel>0
    vel   = U';
    
    % ghost cells required of my artificial boundary conditions:
    % non-reflecting Neumann type boundary conditions are implemented
    vargh = [var(1); var; var(NN)];
    velgh = [vel(1); vel; vel(NN)];
    theta = ones(NN+2,1);
    theta(velgh<0) = -1;
    
    % calculate slopes for the flux limiter (phi)
    TVD_r     = vargh(2:end);
    TVD_r2    = [vargh(3:end); vargh(end)];
    TVD_m     = vargh(1:end-1);
    TVD_l     = [vargh(1); vargh(1:end-2)];
    
    r_TVDup   = (TVD_r2-TVD_r)./(TVD_r-TVD_m);
    r_TVDdown = (TVD_m-TVD_l)./(TVD_r-TVD_m);
    
    r_TVD = r_TVDdown;
    r_TVD(theta(2:end)<0) = r_TVDup(theta(2:end)<0);
    r_TVD(diff(TVD_m)==0) = 1;
    r_TVD(1) = 1;
    r_TVD(end) = 1;
    
%     l_TVDup    = (TVD_m-TVD_l)./(TVD_r-TVD_m);
%     l_TVDdown  = (TVD_r2-TVD_r)./(TVD_r-TVD_m);
%     
%     l_TVD = l_TVDdown;
%     l_TVD(theta(1:end-1)<0) = l_TVDup(theta(1:end-1)<0);
%     l_TVD(diff(TVD_r)==0) = 1;
               
    % define Flux Limiter function (Van Leer)
    phi = (r_TVD + abs(r_TVD))./(1 + abs(r_TVD));
%    phi_l = (l_TVD + abs(l_TVD))./(1 + abs(l_TVD));
    phi_r = phi(2:end);
    phi_l = phi(1:end-1);
    
    % think about my ghost cells
    TVD_r = vargh(3:end);
    TVD_l = vargh(1:end-2);
    
    % compute fluxes for TVD
    F_rl = .5*((1+theta(2:end-1)).*vel.*var + (1-theta(2:end-1)).*vel.*TVD_r);
    F_rh = .5*vel.*(var + TVD_r) - .5*vel.*vel.*dt/dz.*(TVD_r-var);
    
    F_ll = .5*((1+theta(2:end-1)).*vel.*TVD_l + (1-theta(2:end-1)).*vel.*var);
    F_lh = .5*vel.*(TVD_l+var) - .5*vel.*vel.*dt/dz.*(var-TVD_l);
    
    % do the job
    F_right = F_rl + phi_r.*(F_rh - F_rl);
    F_left  = F_ll + phi_l.*(F_lh - F_ll);
    
    vari = var - dt*(F_right-F_left)/dz;
    
    
    if any(~isreal(vari))
        disp('imaginary number = time step problem?')
        params.iterate = 0;
        return
    end

else
    xi = x - U*dt;
    xi(xi>max(x))  = max(x);
    xi(xi<min(x))  = min(x);
    vari = interp1(x,var,xi,method)';
end
end

%========================================================================
function vari = diffusion_1d(var,dz,dt,t)
%========================================================================
global NN params

if (t == 1)
    params.diffA = sparse(diag((1-params.dt/(dz*dz))*ones(NN,1)) +...
        diag(.5*params.dt/(dz*dz)*ones(NN-1,1),1) +...
        diag(.5*params.dt/(dz*dz)*ones(NN-1,1),-1));
    A = params.diffA;
elseif (dt ~= params.dt)
    A = sparse(diag((1-dt/(dz*dz))*ones(NN,1)) +...
        diag(.5*dt/(dz*dz)*ones(NN-1,1),1) +...
        diag(.5*dt/(dz*dz)*ones(NN-1,1),-1));
else
    A = params.diffA;
end
% boundary conditions
% top zero gradient
% A(1,2)     = 2*A(1,2);
% top fixed value
A(1,1)     = 1;
A(1,2)     = 0;
% bottom fixed value
A(NN,NN)   = 1;
A(NN,NN-1) = 0;
vari = A*var;
end

%========================================================================
function Ts = get_Ts(P,F)
%========================================================================
global NN params;

adiabatic     = params.alpha*params.T0/(params.rhom*params.Cp);
dTdF_P        = params.dTdF_Ps*ones(NN,1);
dTdF_P(F>0.2) = params.dTdF_Ph;
Ts            = params.Ts0+dTdF_P.*F+(params.dTdP_F+adiabatic)*P;
end

%========================================================================
function [T,dF,F,Xwater] = melting(T,P,F,Xwater)
%========================================================================
global NN params;

% Solidus
Ts_dry     = get_Ts(P,F);
Ts_wet     = Ts_dry + params.aX.*(0.01.*Xwater).^params.bX;

% Calculate melt due to temperature > Ts
dTdF        = params.dTdF_Ps*ones(NN,1);
dTdF(F>0.2) = params.dTdF_Ph;

ind         = Xwater > 1e-6;
if any(ind)
    % Calculate the water effect on the slope of dTs_dF, i.e. dTs_dF(X)
    % 1) slope (derivative) of X(F)
    % dXdF   = -Xwater(ind).*(1./Dwater-1).*(1-F(ind)).^(1./Dwater-2);
    dXdF    = -Xwater(ind).*(1/params.Dbulk-1).*(1-F(ind)).^(1/params.Dbulk-2);
    % 2) slope of Ts(X)
    dTdX   = params.bX*params.aX.*(0.01.*Xwater(ind)).^(params.bX-1);
    % 3) combined to get slope of Ts( F(X) )
    dTdF_X = dTdX.*dXdF;
    % Add water effect to depletion dependence
    dTdF(ind,1) = dTdF(ind,1) + dTdF_X;
end

latent_heat      = params.T0*T*params.deltaS/params.Cp;
deltaT           = params.T0*T - Ts_wet;
deltaT(deltaT<0) = 0;
dF               = deltaT./(latent_heat + dTdF);

% update the water composition
Xwater = Xwater./(1 + dF.*(1./params.Dbulk - 1));

F = F + dF;
T = T - latent_heat.*dF./params.T0;

end


%========================================================================
% MELT COMPOSITION
%========================================================================
function [Rcomp,Mcomp,num] = setupComp()
%========================================================================
% Solid mantle composition
global NN
num    = 17;
Mcomp  = zeros(num,NN);
%         La     Ce     Pr     Nd     Sm     Eu     Gd     Tb     Dy     Ho     Er     Tm     Yb     Lu     C      Nb     Zr
Rcomp1 = [0.206; 0.772; 0.143; 0.815; 0.299; 0.115; 0.419; 0.077; 0.525; 0.120; 0.347; 0.054; 0.347; 0.054; 0.285; 0.0;   0.0];
Prim   = [0.687; 1.775; 0.276; 1.354; 0.444; 0.168; 0.596; 0.108; 0.737; 0.164; 0.480; 0.074; 0.493; 0.074; 0.285; 0.713; 11.2];
Depl   = [0.234; 0.772; 0.131; 0.713; 0.270; 0.107; 0.395; 0.075; 0.533; 0.122; 0.371; 0.060; 0.401; 0.063; 0.285; 0.390; 7.190];
Rcomp  = repmat(1.0*Prim+0.0*Depl,1,NN);
end

%========================================================================
function [CMantle,CMelt] = REEcomp(T,P,coord,dF,CMantle,CMelt)
%========================================================================
% Calculate melt composition
global NN params

num   = 17;
D     = zeros(num,NN);     % Array to store the partition coefficients

indf = find(dF > 0);

% Partition coefficents for the REEs
% Gibson & Geist Supp. Matt. Earth and Planetary Science Letters
% 2010 (+ McK & O'N for plag and spinel)
% Rosenthal et al.,  Earth and Planetary Science Letters 2015 for Carbon
% Brown & Lesher (2016) for Nb and Zr
Dol     = [0.0005 0.0005 0.0008 0.00042 0.0011 0.0016 0.0011...
    0.0015 0.0027 0.0016 0.013 0.0015  0.02  0.02  0.0007  0.005  0.01];
Dopx    = [0.0031 0.004  0.0048 0.012   0.02   0.013  0.0065...
    0.019  0.011  0.026  0.045 0.04    0.08  0.12  0.0003  0.0031 0.01];
Dcpx    = [.049   0.08   0.126  0.178   0.293  0.335  0.35...
    0.403  0.400  0.427  0.420 0.40968 0.400 0.376 0.0005  0.0077 0.12];
Dplag   = [0.27   0.20   0.17   0.14    0.11   0.73   0.066...
    0.06   0.055  0.048  0.041 0.036   0.031 0.025 0.00055 0.0    0.0 ];
Dspinel = [0.01   0.01   0.01   0.01    0.01   0.01   0.01...
    0.01   0.01   0.01   0.01  0.01    0.01  0.01 0.00055  0.086  0.56];
Dgarnet = [0.001  0.005  0.014  0.052   0.250  0.496  0.848...
    1.477  2.200  3.315  4.400 5.495   6.600 7.100 0.0001  0.02   0.32];
Damph   = [0.17   0.26   0.35   0.44    0.76   0.88   0.86...
    0.83   0.78   0.73   0.68  0.64    0.59  0.51 0.00055  0.0    0.0];

% Proportions of minerals in each facies (olovine, opx, cpx and
% then plagiocalse, spinel, garnet or amphibol).
Plag   = [0.636 0.263 0.012 0.089];
Spinel = [0.578 0.270 0.119 0.033];
Garnet = [0.598 0.211 0.079 0.115];
Amph   = [0.599 0.247 0.038 0.116];

% lithostatic pressure (in GPa) but we could use dynamic...
P      = 1e-9*P;

% how deep are we (in km)?
ssz    = 1e-3*params.lz*coord';

% Spinel-out and garnet-in boundaries
Pspinel_out = (params.T0*T+400)/666.7;
Pgarnet_in  = (params.T0*T+533)/666.7;

% Shallow first: if < 25 km  deep we are in the plagioclase
% stability field
[~,nb] = size(D(:,ssz<=25));
D(:,ssz<=25) = repmat(Dol.*Plag(1) + Dopx.*Plag(2) + Dcpx.*Plag(3) + Dplag.*Plag(4),nb,1)';

%  if between stability, linear transition... %
Da   = zeros(num,1);
Db   = zeros(num,1);
m    = zeros(num,1);
c    = zeros(num,1);
Da = Dol.*Plag(1) + Dopx.*Plag(2) + Dcpx.*Plag(3) + Dplag.*Plag(4);
Db = Dol.*Spinel(1) + Dopx.*Spinel(2) + Dcpx.*Spinel(3) + Dspinel.*Spinel(4);
m  = (Db-Da)./10;
c  = Da-m*25;
for i = 1:num
    D(i,ssz>25 & ssz<=35)  = m(i).*ssz(ssz>25 & ssz<=35)+c(i);
end

% if > 35 km and P < spinel-out
[~,nb] = size(D(:,ssz>35 & P<=Pspinel_out));
D(:,ssz>35 & P<=Pspinel_out) = repmat(Dol.*Spinel(1) + Dopx.*Spinel(2) + Dcpx.*Spinel(3) + Dspinel.*Spinel(4),nb,1)';

% again between stability, linear transition... %
Da   = zeros(num,1);
Db   = zeros(num,1);
m    = zeros(1,NN);
c    = zeros(1,NN);
Da = Dol.*Spinel(1) + Dopx.*Spinel(2) + Dcpx.*Spinel(3) + Dspinel.*Spinel(4);
Db = Dol.*Garnet(1) + Dopx.*Garnet(2) + Dcpx.*Garnet(3) + Dgarnet.*Garnet(4);
for i = 1:num
    m = (Db(i)-Da(i))./(Pgarnet_in-Pspinel_out);
    c = Da(i)-m.*Pspinel_out;
    D(i,P>Pspinel_out & P<=Pgarnet_in) = m(P>Pspinel_out & P<=Pgarnet_in).*P(P>Pspinel_out & P<=Pgarnet_in)+c(P>Pspinel_out & P<=Pgarnet_in);
end

% if P > granet_in
[~,nb] = size(D(:,P>Pgarnet_in));
D(:,P>Pgarnet_in) = repmat(Dol.*Garnet(1) + Dopx.*Garnet(2) + Dcpx.*Garnet(3) + Dgarnet.*Garnet(4),nb,1)';

for i = 1:num
    % calculate melt composition from the solid composition
    CMelt(i,indf) = CMantle(i,indf)./D(i,indf);
    % update the solid composition
    CMantle(i,indf) = CMantle(i,indf)./(1 + dF(indf)'.*(1./D(i,indf) - 1));
    
end
% and make sure solid composition is not less than zero
CMantle(CMantle<0) = 0;

end

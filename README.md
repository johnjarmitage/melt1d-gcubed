# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains a 1D melting model written in Matlab.

### How do I get set up? ###

The matlab script should run on any version of MAtlab. I use 2013.

### Contribution guidelines ###

Just let me know if you spot anything wrong with it.

### Who do I talk to? ###

The owner is John Armitage (armitage@ipgp.fr)